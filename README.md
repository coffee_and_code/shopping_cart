## Shopping Cart Project

# Written with
- Maven 3.0.5
- Java 1.8
- Debian Jessie

# Setup
- Update `/src/main/resources/application.properties` with your database settings.

# Testing
- `mvn test`

# Run server
- `mvn spring-boot:run`

# Notes

- The quantity of each product in the cart is tracked using an intermediary `CartProduct` class, which contains a `Product` and its quantity.  The `Cart` class manages a `Set` of `CartProducts`.
- If a request is received to add a product that is already in the cart, its quantity is incremented.
- A first pass on removing products from the cart is present, however it isn't complete yet.
- The intended behaviour for removing products mirrors that of adding products: if there are multiple instances of the product in the cart, its quantity is decremented.  If there is only one instance, it is simply removed.
