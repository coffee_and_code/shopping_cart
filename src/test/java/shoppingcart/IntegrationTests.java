package shoppingcart;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.google.gson.Gson;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import shoppingcart.models.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    private Gson gson = new Gson();

    @Test
    public void shouldListProducts() throws Exception {
        this.mockMvc.perform(get("/products"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void shouldCreateAndGetProduct() throws Exception {
        Product product = new Product("product");
        String json = gson.toJson(product);

        MvcResult result = this.mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON).content(json))
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.name").value(product.getName()))
            .andReturn();

        Product createdProduct = gson.fromJson(result.getResponse().getContentAsString(), Product.class);

        this.mockMvc.perform(get("/products/" + createdProduct.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(createdProduct.getName()));
    }

    @Test
    public void shouldReturnNotFoundWithUnknownId() throws Exception {
        this.mockMvc.perform(get("/products/1234"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldListProductsInCart() throws Exception {
        this.mockMvc.perform(get("/cart/products"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldAddProductToCart() throws Exception {
        Product product = new Product(1, "product");
        String json = gson.toJson(product);

        this.mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isCreated());

        this.mockMvc.perform(post("/cart/products/" + product.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].product.name").value(product.getName()));
    }

    @Test
    public void shouldDeleteProductFromCart() throws Exception {
        Product product = new Product(1, "product");
        String json = gson.toJson(product);

        this.mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isCreated());

        this.mockMvc.perform(delete("/cart/products/" + product.getId()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldClearCart() throws Exception {
        this.mockMvc.perform(delete("/cart"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(0));
    }
}
