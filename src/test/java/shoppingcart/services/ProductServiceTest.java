package shoppingcart.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import shoppingcart.dao.ProductDao;
import shoppingcart.models.Product;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
    @InjectMocks
    private ProductService productService = new ProductService();

    @Mock
    private ProductDao productDao;

    @Test
    public void shouldSaveAndGetProduct() throws Exception {
        Product product = new Product(1, "product");
        Mockito.when(productDao.save(product)).thenReturn(product.getId());
        Mockito.when(productDao.getById(product.getId())).thenReturn(product);

        long id = productService.save(product);
        Product retrievedProduct = productService.getById(product.getId());

        assertEquals(product.getId(), id);
        assertEquals(product.getId(), retrievedProduct.getId());
    }
}
