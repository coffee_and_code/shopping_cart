package shoppingcart.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import shoppingcart.dao.CartDao;
import shoppingcart.models.Cart;
import shoppingcart.models.CartProduct;
import shoppingcart.models.Product;

import javax.servlet.http.HttpSession;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CartServiceTest {
    @InjectMocks
    private CartService cartService = new CartService();

    @Mock
    private CartDao cartDao;

    @Mock
    private HttpSession httpSession;

    @Test
    public void shouldReturnExistingCartWhenAvailable() throws Exception {
        Cart cart = new Cart("sessionId for existing cart");
        Mockito.when(httpSession.getId()).thenReturn(cart.getSessionId());
        Mockito.when(cartDao.getBySessionId(cart.getSessionId())).thenReturn(cart);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart retrievedCart = cartService.getOrCreate();

        assertNotNull(retrievedCart);
        assertEquals(cart.getSessionId(), retrievedCart.getSessionId());
    }

    @Test
    public void shouldCreateNewCartWhenNoneAvailable() throws Exception {
        String sessionId = "sessionId for new cart";
        Cart cart = new Cart(sessionId);
        Mockito.when(httpSession.getId()).thenReturn(sessionId);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart retrievedCart = cartService.getOrCreate();

        assertNotNull(retrievedCart);
        assertEquals(sessionId, retrievedCart.getSessionId());
    }

    @Test
    public void shouldSetCartProducts() throws Exception {
        String sessionId = "sessionId";
        Set<CartProduct> cartProducts = new HashSet<CartProduct>();
        Cart cart = new Cart(sessionId, cartProducts);
        cartProducts.add(new CartProduct(1, 1, new Product("product")));
        Mockito.when(httpSession.getId()).thenReturn(sessionId);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart initialCart = cartService.getOrCreate();
        Cart updatedCart = cartService.setCartProducts(cartProducts);

        assertTrue(updatedCart.getCartProducts().size() == initialCart.getCartProducts().size() + 1);
    }

    @Test
    public void shouldAddProductToCart() throws Exception {
        String sessionId = "sessionId";
        Product product = new Product(1, "product");
        Cart cart = new Cart(sessionId);
        Mockito.when(httpSession.getId()).thenReturn(sessionId);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart initialCart = cartService.getOrCreate();
        Cart updatedCart = cartService.addProductToCart(product);

        assertTrue(updatedCart.getCartProducts().size() == initialCart.getCartProducts().size() + 1);

        boolean productInCart = false;

        for (CartProduct cartProduct: updatedCart.getCartProducts()) {
            if (cartProduct.getProduct().getId() == product.getId()) {
                productInCart = true;
                break;
            }
        }

        assertTrue(productInCart);
    }

    @Test
    public void shouldDeleteItemFromCart() throws Exception {
        String sessionId = "sessionId";
        Product product = new Product(1, "product");
        Cart cart = new Cart(sessionId);
        Mockito.when(httpSession.getId()).thenReturn(sessionId);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart initialCart = cartService.addProductToCart(product);
        Cart updatedCart = cartService.deleteProductFromCart(product);

        boolean productInCart = false;

        for (CartProduct cartProduct: updatedCart.getCartProducts()) {
            if (cartProduct.getProduct().getId() == product.getId()) {
                productInCart = true;
                break;
            }
        }

        if (productInCart) {
            assertTrue(updatedCart.getCartProducts().size() == initialCart.getCartProducts().size() - 1);
        }
    }

    @Test
    public void shouldClearCart() throws Exception {
        String sessionId = "sessionId";
        Product product = new Product(1, "product");
        Cart cart = new Cart(sessionId);
        Mockito.when(httpSession.getId()).thenReturn(sessionId);
        Mockito.doNothing().when(cartDao).save(cart);

        Cart initialCart = cartService.addProductToCart(product);
        assertEquals(1, initialCart.getCartProducts().size());

        Cart updatedCart = cartService.clear();
        assertEquals(0, updatedCart.getCartProducts().size());
    }
}