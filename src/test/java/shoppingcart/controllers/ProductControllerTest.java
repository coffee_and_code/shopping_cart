package shoppingcart.controllers;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import shoppingcart.models.Product;
import shoppingcart.services.ProductService;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {
    @InjectMocks
    private ProductController productController = new ProductController();

    @Mock
    ProductService productService;

    @After
    public void resetMocks() {
        Mockito.reset(productService);
    }

    @Test
    public void shouldCreateProduct() throws Exception {
        Product product = new Product(1, "product");
        Mockito.when(productService.save(product)).thenReturn(product.getId());

        ResponseEntity<Product> response = productController.create(product);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(product.getId(), response.getBody().getId());
        assertEquals(product.getName(), response.getBody().getName());
    }

    @Test
    public void shouldGetProduct() throws Exception {
        Product product = new Product(1, "product");
        Mockito.when(productService.getById(product.getId())).thenReturn(product);

        ResponseEntity<Product> response = productController.getById(product.getId());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(product.getId(), response.getBody().getId());
        assertEquals(product.getName(), response.getBody().getName());
    }

    @Test
    public void shouldListProducts() throws Exception {
        List<Product> products = Arrays.asList(
                new Product("product 1"),
                new Product("product 2"),
                new Product("product 3")
        );
        Mockito.when(productService.getAll()).thenReturn(products);

        ResponseEntity<List<Product>> response = productController.list();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().equals(products));
    }
}
