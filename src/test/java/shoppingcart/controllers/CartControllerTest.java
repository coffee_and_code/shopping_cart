package shoppingcart.controllers;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import shoppingcart.models.Cart;
import shoppingcart.models.CartProduct;
import shoppingcart.models.Product;
import shoppingcart.services.CartService;
import shoppingcart.services.ProductService;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CartControllerTest {
    @InjectMocks
    private CartController cartController = new CartController();

    @Mock
    CartService cartService;

    @Mock
    ProductService productService;

    @After
    public void resetMocks() {
        Mockito.reset(cartService);
    }

    @Test
    public void shouldListProducts() throws Exception {
        Product product = new Product("product");
        Set<CartProduct> cartProducts = new HashSet<CartProduct>();
        CartProduct cartProduct = new CartProduct(1, product);
        cartProducts.add(cartProduct);
        Cart cart = new Cart("sessionId", cartProducts);
        Mockito.when(cartService.getOrCreate()).thenReturn(cart);

        ResponseEntity<Set<CartProduct>> response = cartController.products();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().contains(cartProduct));
    }

    @Test
    public void shouldAddProductToCart() throws Exception {
        Product product = new Product("product");
        Set<CartProduct> cartProducts = new HashSet<CartProduct>();
        CartProduct cartProduct = new CartProduct(1, product);
        cartProducts.add(cartProduct);
        Cart cart = new Cart("sessionId", cartProducts);
        Mockito.when(cartService.addProductToCart(product)).thenReturn(cart);
        Mockito.when(productService.getById(product.getId())).thenReturn(product);

        ResponseEntity<Set<CartProduct>> response = cartController.addProduct(product.getId());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().contains(cartProduct));
    }

    @Test
    public void shouldReturnBadRequestWhenProductDoesntExist() {
        long productId = 1234;
        Mockito.when(productService.getById(productId)).thenReturn(null);
        ResponseEntity<Set<CartProduct>> response = cartController.addProduct(productId);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
