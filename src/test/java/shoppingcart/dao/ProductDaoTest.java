package shoppingcart.dao;

import org.springframework.test.annotation.DirtiesContext;
import shoppingcart.models.Product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductDaoTest {
    @Autowired
    private ProductDao productDao = new ProductDao();

    @Test
    public void shouldSaveAndGetProduct() throws Exception {
        Product product = new Product("product");
        long id = productDao.save(product);

        Product retrievedProduct = productDao.getById(id);

        assertNotNull(retrievedProduct);
        assertEquals(id, retrievedProduct.getId());
        assertEquals(product.getName(), retrievedProduct.getName());
    }

    @Test
    public void shouldListProducts() throws Exception {
        List<Product> retrievedProducts = productDao.getAll();

        assertEquals(0, retrievedProducts.size());

        List<Product> products = Arrays.asList(
            new Product("product 1"),
            new Product("product 2"),
            new Product("product 3")
        );

        for (int i=0; i<products.size(); i++) {
            productDao.save(products.get(i));
        }

        retrievedProducts = productDao.getAll();

        assertEquals(products.size(), retrievedProducts.size());

        for (int i=0; i<products.size(); i++) {
            assertEquals(products.get(i).getName(), retrievedProducts.get(i).getName());
        }
    }
}
