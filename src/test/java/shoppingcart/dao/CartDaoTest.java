package shoppingcart.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import shoppingcart.models.Cart;
import shoppingcart.models.CartProduct;
import shoppingcart.models.Product;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CartDaoTest {

    @Autowired
    private CartDao cartDao = new CartDao();

    @Test
    public void shouldSaveAndGetCart() throws Exception {
        String sessionId = "sessionId";
        cartDao.save(new Cart(sessionId));
        Cart retrievedCart = cartDao.getBySessionId(sessionId);

        assertNotNull(retrievedCart);
        assertEquals(retrievedCart.getSessionId(), sessionId);
    }

    @Test
    public void shouldUpdateCart() throws Exception {
        String sessionId = "sessionId";
        Cart cart = new Cart(sessionId);
        cartDao.save(cart);

        Set<CartProduct> cartProducts = new HashSet<CartProduct>();
        CartProduct cartProduct = new CartProduct(1, new Product("new product"));
        cartProducts.add(cartProduct);
        cart.setCartProducts(cartProducts);

        cartDao.update(cart);
        Cart updatedCart = cartDao.getBySessionId(sessionId);

        assertNotNull(updatedCart);
        assertEquals(cart.getSessionId(), updatedCart.getSessionId());
        assertTrue(cart.getCartProducts().contains(cartProduct));
    }
}
