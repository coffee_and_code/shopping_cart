package shoppingcart.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shoppingcart.dao.CartDao;
import shoppingcart.models.Cart;
import shoppingcart.models.CartProduct;
import shoppingcart.models.Product;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Service
public class CartService {

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private CartDao cartDao;

    /**
     *
     * Get or create cart.  Carts are identified by the HTTP session id.  If
     * one exists for the current session, that will be used.  If not, a new
     * one is created.
     *
     * @return  The cart for the current session.
     */
    public Cart getOrCreate() {
        String sessionId = httpSession.getId();
        Cart currentCart = cartDao.getBySessionId(sessionId);

        if (currentCart == null) {
            Cart newCart = new Cart(sessionId);
            cartDao.save(newCart);
            return newCart;
        } else {
            return currentCart;
        }
    }

    /**
     *
     * Replaces all cart contents with the provided set of products.
     *
     * @param cartProducts  The new set of products for the cart.
     * @return              The updated cart.
     */
    public Cart setCartProducts(Set<CartProduct> cartProducts) {
        Cart cart = this.getOrCreate();
        cart.setCartProducts(cartProducts);
        cartDao.update(cart);
        return cart;
    }

    /**
     *
     * Adds a single product to the cart.  If the product is already present,
     * its quantity is updated.  If not, it's added with a quantity of 1.
     *
     * @param product   Product to add to cart.
     * @return          Updated cart.
     */
    public Cart addProductToCart(Product product) {
        Cart cart = this.getOrCreate();
        Set<CartProduct> cartProducts = cart.getCartProducts();
        boolean foundProductInCart = false;

        // search for product in existing cart contents
        for (CartProduct cartProduct: cartProducts) {
            if (cartProduct.getProduct().getId() == product.getId()) {
                // found product, increment quantity
                cartProduct.setQuantity(cartProduct.getQuantity() + 1);
                foundProductInCart = true;
                break;
            }
        }

        // product not in cart yet, add it
        if (!foundProductInCart) {
            cartProducts.add(new CartProduct(1, product));
        }

        cart.setCartProducts(cartProducts);
        cartDao.update(cart);
        return cart;
    }

    /**
     *
     * Delete a single product from the cart. If the product is in the cart, its
     * quantity is decremented.  If it's decremented to 0 or lower, it's removed
     * from the cart altogether.  If the product is not present in the cart at
     * all, nothing is done.
     *
     * @param product   Product to remove from the cart.
     * @return          Updated cart.
     */
    public Cart deleteProductFromCart(Product product) {
        Cart cart = this.getOrCreate();
        Set<CartProduct> cartProducts = cart.getCartProducts();

        // search for product in cart contents
        for (Iterator<CartProduct> i = cartProducts.iterator(); i.hasNext();) {
            CartProduct cartProduct = i.next();
            if (cartProduct.getProduct().getId() == product.getId()) {
                // found product, decrement quantity and remove if needed
                int quantity = cartProduct.getQuantity() - 1;

                if (quantity <= 0) {
                    i.remove();
                } else {
                    cartProduct.setQuantity(quantity);
                }

                break;
            }
        }

        cart.setCartProducts(cartProducts);
        return cart;
    }

    /**
     *
     * Clear all cart contents.
     *
     * @return  The cart with an empty set of products.
     */
    public Cart clear() {
        return this.setCartProducts(new HashSet<CartProduct>());
    }
}