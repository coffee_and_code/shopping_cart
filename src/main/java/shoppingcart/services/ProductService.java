package shoppingcart.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shoppingcart.dao.ProductDao;
import shoppingcart.models.Product;

import java.util.Arrays;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductDao productDao;

    /**
     *
     * Save given product in repository.
     *
     * @param product   Product to save.
     * @return          Database id of saved product.
     */
    public long save(Product product) {
        return productDao.save(product);
    }

    /**
     *
     * Retrieve the product with the given database id from the repository.
     *
     * @param id    Database id to use for retrieval.
     * @return      Product with given database id, or null if not found.
     */
    public Product getById(long id) {
        return productDao.getById(id);
    }

    /**
     *
     * List all products in repository.
     *
     * @return  List of products in repository.
     */
    public List<Product> getAll() {
        return productDao.getAll();
    }
}
