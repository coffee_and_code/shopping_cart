package shoppingcart.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="carts", uniqueConstraints=@UniqueConstraint(columnNames="sessionId"))
public class Cart {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(unique=true)
    private String sessionId;

    @OneToMany(targetEntity=CartProduct.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @NotNull
    private Set<CartProduct> cartProducts = new HashSet<CartProduct>();

    public Cart() {}

    public Cart(String sessionId) {
        this.sessionId = sessionId;
    }

    public Cart(Set<CartProduct> cartProducts) {
        this.cartProducts = cartProducts;
    }

    public Cart(String sessionId, Set<CartProduct> cartProducts) {
        this.sessionId = sessionId;
        this.cartProducts = cartProducts;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Set<CartProduct> getCartProducts() {
        return this.cartProducts;
    }

    public void setCartProducts(Set<CartProduct> cartProducts) {
        this.cartProducts = cartProducts;
    }
}
