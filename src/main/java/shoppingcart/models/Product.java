package shoppingcart.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="products")
public class Product {

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @NotNull
  @Size(min = 2, max = 80)
  private String name;

  // ------------------------
  // PUBLIC METHODS
  // ------------------------

  public Product() {}

  public Product(long id) {
    this.id = id;
  }

  public Product(String name) {
    this.name = name;
  }

  public Product(long id, String name) {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long value) {
    this.id = value;
  }
  
  public String getName() {
    return name;
  }

  public void setName(String value) {
    this.name = value;
  }
} // class Product
