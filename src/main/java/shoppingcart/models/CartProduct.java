package shoppingcart.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="cartproducts")
public class CartProduct {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @OneToOne(targetEntity=Product.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @NotNull
    private Product product;

    @NotNull
    private int quantity = 1;

    public CartProduct() {}

    public CartProduct(long id) {
        this.id = id;
    }

    public CartProduct(int quantity) {
        this.quantity = quantity;
    }

    public CartProduct(Product product) {
        this.product = product;
    }

    public CartProduct(long id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public CartProduct(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public CartProduct(long id, int quantity, Product product) {
        this.id = id;
        this.quantity = quantity;
        this.product = product;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
