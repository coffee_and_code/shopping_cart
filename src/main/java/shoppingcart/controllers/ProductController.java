package shoppingcart.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shoppingcart.models.Product;

import org.springframework.beans.factory.annotation.Autowired;
import shoppingcart.services.ProductService;

import java.util.List;

@RestController
@RequestMapping(value="/products")
public class ProductController {

  @Autowired
  private ProductService productService;

    /**
     *
     * API endpoint for returning all products.
     *
     * @return  The list of products.
     */
    @RequestMapping(value="", method=RequestMethod.GET)
    public ResponseEntity<List<Product>> list() {
        return new ResponseEntity<List<Product>>(productService.getAll(), HttpStatus.OK);
    }

    /**
     *
     * API endpoint for creating a new product. Requires Content-Type: application/json header.
     *
     * @param product   The product to be created.
     * @return          The newly-created product, including the database id.
     */
    @RequestMapping(value="", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> create(@RequestBody Product product) {
        productService.save(product);
        return new ResponseEntity<Product>(product, HttpStatus.CREATED);
    }

    /**
     *
     * API endpoint for retrieving a single product by database id.
     *
     * @param id    The database id of the requested product.
     * @return      The requested product, or 404 if none exist with the provided database id.
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Product> getById(@PathVariable("id") long id) {
        Product product = productService.getById(id);

        if (product == null) {
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Product>(product, HttpStatus.OK);
        }
    }
} // class ProductController
