package shoppingcart.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shoppingcart.models.Cart;
import shoppingcart.models.CartProduct;
import shoppingcart.models.Product;
import shoppingcart.services.CartService;
import shoppingcart.services.ProductService;

import java.util.Set;

@RestController
@RequestMapping(value="/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    /**
     *
     * API endpoint for returning current cart state.
     *
     * @return  Set of products currently in cart.
     */
    @RequestMapping(value="/products", method=RequestMethod.GET)
    public ResponseEntity<Set<CartProduct>> products() {
        Cart cart = cartService.getOrCreate();
        return new ResponseEntity<Set<CartProduct>>(cart.getCartProducts(), HttpStatus.OK);
    }

    /**
     *
     * API endpoint for adding a product to cart.
     *
     * @param productId     The database id of the product to add.
     * @return              The updated set of products in cart.
     */
    @RequestMapping(value="/products/{productId}", method=RequestMethod.POST)
    public ResponseEntity<Set<CartProduct>> addProduct(@PathVariable("productId") long productId) {
        Product product = productService.getById(productId);

        if (product == null) {
            return new ResponseEntity<Set<CartProduct>>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<Set<CartProduct>>(cartService.addProductToCart(product).getCartProducts(), HttpStatus.OK);
        }
    }

    /**
     *
     * API endpoint for removing a product from cart.
     *
     * @param productId     The database id of the product to remove.
     * @return              The updated set of products in cart.
     */
    @RequestMapping(value="/products/{productId}", method=RequestMethod.DELETE)
    public ResponseEntity deleteProduct(@PathVariable("productId") long productId) {
        Product product = productService.getById(productId);

        if (product == null) {
            return new ResponseEntity<Set<CartProduct>>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<Set<CartProduct>>(cartService.deleteProductFromCart(product).getCartProducts(), HttpStatus.OK);
        }
    }

    /**
     *
     * API endpoint for clearing all cart contents.
     *
     * @return  The updated (empty) set of products in cart.
     */
    @RequestMapping(value="", method=RequestMethod.DELETE)
    public ResponseEntity<Set<CartProduct>> clear() {
        return new ResponseEntity<Set<CartProduct>>(cartService.clear().getCartProducts(), HttpStatus.OK);
    }
}