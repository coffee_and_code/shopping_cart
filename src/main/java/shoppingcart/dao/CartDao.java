package shoppingcart.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import shoppingcart.models.Cart;

@Repository
@Transactional
public class CartDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     *
     * Save given cart in database.
     *
     * @param cart  Cart to save in database.
     */
    public void save(Cart cart) {
        getSession().save(cart);
    }

    /**
     *
     * Retrieve cart with given session id from database.
     *
     * @param sessionId Session id to use in retrieval.
     * @return          Session with given id, or null in none exists.
     */
    public Cart getBySessionId(String sessionId) {
        return (Cart) getSession()
                        .createQuery("from Cart where sessionId = :sessionId")
                        .setParameter("sessionId", sessionId)
                        .uniqueResult();
    }

    /**
     *
     * Update cart with given data.
     *
     * @param cart  Cart to update.
     */
    public void update(Cart cart) {
        getSession().update(cart);
    }
}
