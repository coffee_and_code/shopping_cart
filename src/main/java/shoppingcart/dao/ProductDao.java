package shoppingcart.dao;

import javax.transaction.Transactional;
import shoppingcart.models.Product;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
@Transactional
public class ProductDao {
  
  @Autowired
  private SessionFactory sessionFactory;
  
  private Session getSession() {
    return sessionFactory.getCurrentSession();
  }

    /**
     *
     * Save given product in database.
     *
     * @param product   Product to save in database.
     * @return          Database id of saved product.
     */
  public long save(Product product) {
      getSession().save(product);
      return product.getId();
  }

    /**
     *
     * Retrieve product with given id from database.
     *
     * @param id    Database id to use in retrieval.
     * @return      Product with given id, or null if none exists.
     */
  public Product getById(long id) {
      return (Product) getSession().get(Product.class, id);
  }

    /**
     *
     * Retrieve all products from database.
     *
     * @return  List of all products in database.
     */
  public List<Product> getAll() {
      return getSession().createQuery("from Product").list();
  }
} // class ProductDao
